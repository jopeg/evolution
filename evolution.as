﻿package 
{
	import flash.display.MovieClip;
	import flash.utils.Timer;
	import flash.events.*;
	import flash.ui.*;
	import flash.display3D.IndexBuffer3D;
	import fl.motion.easing.Back;

	public class evolution extends MovieClip
	{
		public var p1:playa;
		public var ground:platform;
		public var win:food;
		public var red:button;
		
		public var rightBarrier:barrier;
		
		public var speed = 5;
		public var jump = 10;
		public var fall = 10;
		
		public var isFalling = true;
		public var isJumping = false;
		public var canJump = false;
		public var movingRight = false;
		public var movingLeft = false;
		public var starting = true;
		
		public var curLev = 1;
		public var nextLevel = true;

		public var platNum = 0;
		public var platforms = [];
		public var curPlat:platform;
		public var plat1:platform;
		public var plat2:platform;
		public var plat3:platform;
		public var plat4:platform;
		public var plat5:platform;
		public var plat6:platform;
		public var plat7:platform;
		public var plat8:platform;
		public var onPlat = false;
		
		public var levComp = false;
		public var endGame = false;
		public var enterPressed = false;
		
		public var txtCount = 2;
		public var txt:text;
		public var descision = false;
		public var pauseIt = true;
		
		public function evolution()
		{
			p1 = new playa();
			addChild( p1 );			
			
			stage.addEventListener(Event.ENTER_FRAME, update);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyEventHandler);
			stage.addEventListener(KeyboardEvent.KEY_UP, keyEventHandlerUp);

		}
		public function levelCreate(num) 
		{
			trace(num)
			if (num == 1)
			{
				win = new food();
				win.x = 500;
				win.y = 350;
				addChild( win );
				ground = new platform();
				ground.x = 0;
				ground.y = 580
				ground.height = 25;
				ground.width = 814;
				addChild( ground );
				platforms[platNum] = ground;
				platNum++
				plat1 = new platform();
				plat1.x = 300;
				plat1.y = 475;
				plat1.width = 150;
				addChild( plat1 );
				platforms[platNum] = plat1;
				platNum++
			}
			if (num == 2)
			{
				win = new food();
				win.x = 700;
				win.y = 50;
				addChild( win );
				ground = new platform();
				ground.x = 0;
				ground.y = 580
				ground.height = 25;
				ground.width = 814;
				addChild( ground );
				platforms[platNum] = ground;
				platNum++
				plat1 = new platform();
				plat1.x = 650;
				plat1.y = 475;
				plat1.width = 150;
				addChild( plat1 );
				platforms[platNum] = plat1;
				platNum++
				plat2 = new platform();
				plat2.x = 250;
				plat2.y = 350;
				plat2.width = 300;
				addChild( plat2 );
				platforms[platNum] = plat2;
				platNum++
				plat3 = new platform();
				plat3.x = 650;
				plat3.y = 200;
				plat3.width = 100;
				addChild( plat3 );
				platforms[platNum] = plat3;
				platNum++
			}
			if (num == 3)
			{
				win = new food();
				win.x = 125;
				win.y = 150;
				addChild( win );
				ground = new platform();
				ground.x = 0;
				ground.y = 580
				ground.height = 25;
				ground.width = 814;
				addChild( ground );
				platforms[platNum] = ground;
				platNum++
				plat1 = new platform();
				plat1.x = 125;
				plat1.y = 500;
				plat1.width = 150;
				addChild( plat1 );
				platforms[platNum] = plat1;
				platNum++
				plat2 = new platform();
				plat2.x = 300;
				plat2.y = 350;
				plat2.width = 150;
				addChild( plat2 );
				platforms[platNum] = plat2;
				platNum++
				plat3 = new platform();
				plat3.x = 625;
				plat3.y = 200;
				plat3.width = 100;
				addChild( plat3 );
				platforms[platNum] = plat3;
				platNum++
				plat4 = new platform();
				plat4.x = 200;
				plat4.y = 75;
				plat4.width = 500;
				addChild( plat4 );
				platforms[platNum] = plat4;
				platNum++
			}
			if (num == 4)
			{
				win = new food();
				win.x = 400;
				win.y = 50;
				addChild( win );
				ground = new platform();
				ground.x = 0;
				ground.y = 580
				ground.height = 25;
				ground.width = 814;
				addChild( ground );
				platforms[platNum] = ground;
				platNum++
				plat1 = new platform();
				plat1.x = 400;
				plat1.y = 500;
				plat1.width = 150;
				addChild( plat1 );
				platforms[platNum] = plat1;
				platNum++
				plat2 = new platform();
				plat2.x = 250;
				plat2.y = 375;
				plat2.width = 150;
				addChild( plat2 );
				platforms[platNum] = plat2;
				platNum++
				plat3 = new platform();
				plat3.x = 550;
				plat3.y = 375;
				plat3.width = 150;
				addChild( plat3 );
				platforms[platNum] = plat3;
				platNum++
				plat4 = new platform();
				plat4.x = 400;
				plat4.y = 225;
				plat4.width = 250;
				addChild( plat4 );
				platforms[platNum] = plat4;
				platNum++
			}
			if (num == 5)
			{
				win = new food();
				win.x = 700;
				win.y = 50;
				addChild( win );
				ground = new platform();
				ground.x = 0;
				ground.y = 580
				ground.height = 25;
				ground.width = 814;
				addChild( ground );
				platforms[platNum] = ground;
				platNum++
				plat1 = new platform();
				plat1.x = 200;
				plat1.y = 450;
				plat1.width = 100;
				addChild( plat1 );
				platforms[platNum] = plat1;
				platNum++
				plat2 = new platform();
				plat2.x = 400;
				plat2.y = 300;
				plat2.width = 150;
				addChild( plat2 );
				platforms[platNum] = plat2;
				platNum++
				plat3 = new platform();
				plat3.x = 200;
				plat3.y = 100;
				plat3.width = 300;
				addChild( plat3 );
				platforms[platNum] = plat3;
				platNum++
				plat4 = new platform();
				plat4.x = 400;
				plat4.y = 400;
				plat4.width = 100;
				addChild( plat4 );
				platforms[platNum] = plat4;
				platNum++
				
			}
			if (num == 6)
			{
				win = new food();
				win.x = 400;
				win.y = 125;
				addChild( win );
				ground = new platform();
				ground.x = 0;
				ground.y = 580
				ground.height = 25;
				ground.width = 814;
				addChild( ground );
				platforms[platNum] = ground;
				platNum++
				plat1 = new platform();
				plat1.x = 200;
				plat1.y = 550;
				plat1.width = 50;
				addChild( plat1 );
				platforms[platNum] = plat1;
				platNum++
				plat2 = new platform();
				plat2.x = 250;
				plat2.y = 500;
				plat2.width = 50;
				addChild( plat2 );
				platforms[platNum] = plat2;
				platNum++
				plat3 = new platform();
				plat3.x = 300;
				plat3.y = 450;
				plat3.width = 50;
				addChild( plat3 );
				platforms[platNum] = plat3;
				platNum++
				plat4 = new platform();
				plat4.x = 350;
				plat4.y = 400;
				plat4.width = 50;
				addChild( plat4 );
				platforms[platNum] = plat4;
				platNum++
				plat5 = new platform();
				plat5.x = 550;
				plat5.y = 550;
				plat5.width = 50;
				addChild( plat5 );
				platforms[platNum] = plat5;
				platNum++
				plat6 = new platform();
				plat6.x = 500;
				plat6.y = 500;
				plat6.width = 50;
				addChild( plat6 );
				platforms[platNum] = plat6;
				platNum++
				plat7 = new platform();
				plat7.x = 450;
				plat7.y = 450;
				plat7.width = 50;
				addChild( plat7 );
				platforms[platNum] = plat7;
				platNum++
				plat8 = new platform();
				plat8.x = 400;
				plat8.y = 400;
				plat8.width = 50;
				addChild( plat8 );
				platforms[platNum] = plat8;
				platNum++
			}
			if (num == 7)
			{
				p1.height = 150;
				p1.width = 200;
				endGame = true;
				red = new button();
				addChild( red )
				txt = new text();
				txt.gotoAndStop(1);
				addChild( txt );
				ground = new platform();
				ground.x = 0;
				ground.y = 580
				ground.height = 25;
				ground.width = 814;
				addChild( ground );
				platforms[platNum] = ground;
				platNum++
			}
		}
		public function update( e : Event )
		{			
			if (endGame == true)
			{
				if (p1.hitTestObject( red ) == true)
				{
					pauseIt = false;
				}
			}
			if (p1.y <= 0)
			{
				p1.y += 5;
			}
			if (nextLevel == true)
			{
				trace('next')
				gotoAndStop(curLev);
				levelCreate(curLev);
				p1.gotoAndStop(curLev);
				rightBarrier = new barrier();
				addChild( rightBarrier );
				curLev++;
				nextLevel = false;
			}
			if (p1.hitTestObject( rightBarrier ) == true)
			{
				p1.x -= 5;
				if (levComp == true)
				{
					trace('win!')
					nextLevel = true;
					p1.x = 0
					p1.y = 0
					trace(p1.x,p1.y);
					isFalling = true;
					starting = true;
					platNum = 0;
					for each (var pl in platforms)
					{
						pl.x = -9999;
						pl.y = -9999;
						removeChild(pl);
					}
					levComp = false;
				}
			}
			if (p1.x == 0)
			{
				p1.x += 5;
			}
			if (movingRight == true)
			{
				p1.x += speed;
			}
			if (movingLeft == true)
			{
				p1.x -= speed;
			}
			if (isJumping == true)
			{
				p1.y -= jump;
				jump -= .5;
				if ( jump <= 0 )
				{
					isJumping = false;
					isFalling = true;
					jump += 12;
				}
				for each (var plat in platforms)
				{
					if (p1.hitTestObject( plat ) == true)
					{
						canJump = true;
						onPlat = true;
						isFalling = false;
						curPlat = plat;
					}
				}
			}
			if (onPlat == true)
			{
				if (p1.hitTestObject( curPlat ) == false)
				{
					canJump = false;
					isFalling = true;
					onPlat = false;
				}
			}
			if (isFalling == true)
			{
				p1.y += fall
				for each (var plat in platforms)
				{
					if (p1.hitTestObject( plat ) == true)
					{
						canJump = true;
						onPlat = true;
						isFalling = false;
						curPlat = plat;
					}
				}
			}
			if (p1.hitTestObject( ground ) == true)
			{
				curPlat = ground;
				isFalling = false;
				canJump = true;
				starting = false;
			}
			if (p1.hitTestObject( win ) == true)
			{
				var win:food = win;
				removeChild(win);
				levComp = true;
			}
			if (starting == true)
			{
				movingRight = false;
				movingLeft = false;
			}
		}
		public function keyEventHandler( e : KeyboardEvent ):void 
		{
			if (e.keyCode == Keyboard.RIGHT && starting == false)
			{
				movingRight = true;
			}
			if (e.keyCode == Keyboard.LEFT && starting == false)
			{
				movingLeft = true;
			}
			if (e.keyCode == Keyboard.SPACE && canJump == true)
			{
				canJump = false;
				isJumping = true;
				isFalling = false;
				onPlat = false;
				
			}
			if (e.keyCode == Keyboard.F && endGame == true)
			{
				if (txtCount == 5)
				{
					endTheGame();
				}
				txt.gotoAndStop(txtCount);
				txtCount++
			}
		}
		public function endTheGame()
		{
			p1.x = -9999;
			ground.x = -9999;
			txt.x = -9999;
			red.x = -9999;
			gotoAndPlay(8);
		}
		public function keyEventHandlerUp( e : KeyboardEvent ):void 
		{
			if (e.keyCode == Keyboard.RIGHT)
			{
				movingRight = false;
			}
			if (e.keyCode == Keyboard.LEFT)
			{
				movingLeft = false;
			}
		}
	}
}